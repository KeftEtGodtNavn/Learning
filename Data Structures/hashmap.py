# Initialise hashmap.
city_map = {}

# Set our cities (keys) as cities in canada.
cities = ['Calgary', 'Vancouver', 'Toronto']

# Append cities to our hashmap.
city_map['Canada'] = []
city_map['Canada'] += cities

# Show our hash map.
print(city_map)